<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Resources',
	'Resources'
);

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Stylence.' . $_EXTKEY,
		'web',	 // Make module a submodule of 'web'
		'resources',	// Submodule key
		'',						// Position
		array(
			'Resource' => 'list, show, new, create, edit, update, delete',
			'ImageResource' => 'list, show, new, create, edit, update, delete',
			'FileResource' => 'list, show, new, create, edit, update, delete',
			'VideoResource' => 'list, show, new, create, edit, update, delete',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_resources.xlf',
		)
	);

}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Simply Resource');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_simplyresources_domain_model_resource', 'EXT:simply_resources/Resources/Private/Language/locallang_csh_tx_simplyresources_domain_model_resource.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_simplyresources_domain_model_resource');
$TCA['tx_simplyresources_domain_model_resource'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_resource',
		'label' => 'resource_type',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		// this tells extbase to respect the "permit_type" column
		'type' => 'resource_type',
		'searchFields' => 'resource_type,description,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Resource.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_simplyresources_domain_model_resource.gif'
	),
);

?>