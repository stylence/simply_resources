<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$accessTab = '--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, starttime, endtime';
$TCA['tx_simplyresources_domain_model_resource'] = array(
	'ctrl' => $TCA['tx_simplyresources_domain_model_resource']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, resource_type, description',
	),
	'types' => array(
		'0' => array('showitem' => 'resource_type, '),
		// Image
		'1' => array('showitem' => 'resource_type, image, alternative, caption, link, description,' . $accessTab),
		// File
		'2' => array('showitem' => 'resource_type, file, file_name, description,' . $accessTab),
		// Video
		'3' => array('showitem' => 'resource_type, video_reference, description,' . $accessTab),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_simplyresources_domain_model_resource',
				'foreign_table_where' => 'AND tx_simplyresources_domain_model_resource.pid=###CURRENT_PID### AND tx_simplyresources_domain_model_resource.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'resource_type' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_resource.resource_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('-- Label --', 0),
					array('LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_imageresource', 1),
					array('LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_fileresource', 2),
					array('LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_videoresource', 3),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => 'required'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_resource.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'file' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_fileresource.file',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'uploadfolder' => 'uploads/tx_simplyresources/files',
				'allowed' => '*',
				'disallowed' => 'php',
				'size' => 1,
				'maxitems' => 1,
			),
		),
		'file_name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_fileresource.file_name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'image' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_imageresource.image',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'uploadfolder' => 'uploads/tx_simplyresources/images',
				'show_thumbs' => 1,
				'size' => 1,
				'maxitems' => 1,
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
				'disallowed' => '',
			),
		),
		'caption' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_imageresource.caption',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'alternative' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_imageresource.alternative',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'link' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_imageresource.link',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'video_reference' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:simply_resources/Resources/Private/Language/locallang_db.xlf:tx_simplyresources_domain_model_videoresource.video_reference',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
	),
);

?>