<?php
namespace Stylence\SimplyResources\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package simply_resources
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImageResourceController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * imageResourceRepository
	 *
	 * @var \Stylence\SimplyResources\Domain\Repository\ImageResourceRepository
	 * @inject
	 */
	protected $imageResourceRepository;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$imageResources = $this->imageResourceRepository->findAll();
		$this->view->assign('imageResources', $imageResources);
	}

	/**
	 * action show
	 *
	 * @param \Stylence\SimplyResources\Domain\Model\ImageResource $imageResource
	 * @return void
	 */
	public function showAction(\Stylence\SimplyResources\Domain\Model\ImageResource $imageResource) {
		$this->view->assign('imageResource', $imageResource);
	}

	/**
	 * action new
	 *
	 * @param \Stylence\SimplyResources\Domain\Model\ImageResource $newImageResource
	 * @dontvalidate $newImageResource
	 * @return void
	 */
	public function newAction(\Stylence\SimplyResources\Domain\Model\ImageResource $newImageResource = NULL) {
		$this->view->assign('newImageResource', $newImageResource);
	}

	/**
	 * action create
	 *
	 * @param \Stylence\SimplyResources\Domain\Model\ImageResource $newImageResource
	 * @return void
	 */
	public function createAction(\Stylence\SimplyResources\Domain\Model\ImageResource $newImageResource) {
		$this->imageResourceRepository->add($newImageResource);
		$this->flashMessageContainer->add('Your new ImageResource was created.');
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \Stylence\SimplyResources\Domain\Model\ImageResource $imageResource
	 * @return void
	 */
	public function editAction(\Stylence\SimplyResources\Domain\Model\ImageResource $imageResource) {
		$this->view->assign('imageResource', $imageResource);
	}

	/**
	 * action update
	 *
	 * @param \Stylence\SimplyResources\Domain\Model\ImageResource $imageResource
	 * @dontverifyrequesthash
	 * @return void
	 */
	public function updateAction(\Stylence\SimplyResources\Domain\Model\ImageResource $imageResource) {
		$this->imageResourceRepository->update($imageResource);
		$this->flashMessageContainer->add('Your ImageResource was updated.');
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \Stylence\SimplyResources\Domain\Model\ImageResource $imageResource
	 * @return void
	 */
	public function deleteAction(\Stylence\SimplyResources\Domain\Model\ImageResource $imageResource) {
		$this->imageResourceRepository->remove($imageResource);
		$this->flashMessageContainer->add('Your ImageResource was removed.');
		$this->redirect('list');
	}

}
?>