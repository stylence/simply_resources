<?php
namespace Stylence\SimplyResources\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package simply_resources
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FileResource extends Resource {

	/**
	 * File
	 *
	 * @var \string
	 * @validate NotEmpty
	 */
	protected $file;

	/**
	 * File name
	 *
	 * @var \string
	 */
	protected $fileName;

	/**
	 * Returns the file
	 *
	 * @return \string $file
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * Sets the file
	 *
	 * @param \string $file
	 * @return void
	 */
	public function setFile($file) {
		if(is_array($file)) {
			if($file['error'] === 4) {
				return '';
			} else {
				\Stylence\SimplyResources\Utility\FileUtility::uploadFile($file, 'uploads/tx_simplyresources/files/');
				$this->file = $file['name'];
			}
		} else {
			// It hasn't changed
			$this->file = $this->getFile();
		}
	}

	/**
	 * Returns the fileName
	 *
	 * @return \string $fileName
	 */
	public function getFileName() {
		return $this->fileName;
	}

	/**
	 * Sets the fileName
	 *
	 * @param \string $fileName
	 * @return void
	 */
	public function setFileName($fileName) {
		$this->fileName = $fileName;
	}

}
?>