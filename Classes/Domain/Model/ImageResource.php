<?php
namespace Stylence\SimplyResources\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package simply_resources
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ImageResource extends Resource {

	/**
	 * Image
	 *
	 * @var \string
	 */
	protected $image;

	/**
	 * Caption
	 *
	 * @var \string
	 */
	protected $caption;

	/**
	 * Alternative
	 *
	 * @var \string
	 */
	protected $alternative;

	/**
	 * Link
	 *
	 * @var \string
	 */
	protected $link;

	/**
	 * Returns the image
	 *
	 * @return \string $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \string $image
	 * @return void
	 */
	public function setImage($image) {
		if(is_array($image)) {
			if($image['error'] === 4) {
				return '';
			} else {
				\Stylence\SimplyResources\Utility\FileUtility::uploadFile($image, 'uploads/tx_simplyresources/images/');
				$this->image = $image['name'];
			}
		} else {
			// It hasn't changed
			$this->image = $this->getImage();
		}
	}

	/**
	 * Returns the caption
	 *
	 * @return \string $caption
	 */
	public function getCaption() {
		return $this->caption;
	}

	/**
	 * Sets the caption
	 *
	 * @param \string $caption
	 * @return void
	 */
	public function setCaption($caption) {
		$this->caption = $caption;
	}

	/**
	 * Returns the alternative
	 *
	 * @return \string $alternative
	 */
	public function getAlternative() {
		return $this->alternative;
	}

	/**
	 * Sets the alternative
	 *
	 * @param \string $alternative
	 * @return void
	 */
	public function setAlternative($alternative) {
		$this->alternative = $alternative;
	}

	/**
	 * Returns the link
	 *
	 * @return \string $link
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * Sets the link
	 *
	 * @param \string $link
	 * @return void
	 */
	public function setLink($link) {
		$this->link = $link;
	}

}
?>