<?php
namespace Stylence\SimplyResources\Utility;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

	/**
	 *
	 *
	 * @package simply_resources
	 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
	 *
	 */

	class FileUtility implements \t3lib_Singleton {

		/**
		 * @param array $file
		 * @param string $path
		 * @return void
		 */
		public function uploadFile(array $file, $path) {
			$fileUtility = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Utility\File\BasicFileUtility');
			$fileName = '';
			$fileName = $fileUtility->getUniqueName(
				$file['name'],
				\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($path . $fileName)
			);
			\TYPO3\CMS\Core\Utility\GeneralUtility::upload_copy_move($file['tmp_name'], $fileName);
		}
	}