<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Stylence.' . $_EXTKEY,
	'Resources',
	array(
		'Resource' => 'list, show, new, create, edit, update, delete',
		'ImageResource' => 'list, show, new, create, edit, update, delete',
		'FileResource' => 'list, show, new, create, edit, update, delete',
		'VideoResource' => 'list, show, new, create, edit, update, delete',

	),
	// non-cacheable actions
	array(
		'Resource' => 'create, update, delete',
		'ImageResource' => 'create, update, delete',
		'FileResource' => 'create, update, delete',
		'VideoResource' => 'create, update, delete',

	)
);

?>